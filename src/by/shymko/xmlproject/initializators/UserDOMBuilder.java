package by.shymko.xmlproject.initializators;

import by.shymko.xmlproject.exceptions.LogicalException;
import by.shymko.xmlproject.goods.Good;
import by.shymko.xmlproject.list.ClientList;
import by.shymko.xmlproject.list.GoodsList;
import by.shymko.xmlproject.user.Administrator;
import by.shymko.xmlproject.user.Client;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;

/**
 * Created by Andrey on 10.03.2015.
 */
public class UserDOMBuilder extends AbstractUserBuilder{
    private static final Logger LOG = Logger.getLogger(UserDOMBuilder.class);
    private DocumentBuilder docBuilder;

    public UserDOMBuilder() {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try {
            docBuilder = factory.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            LOG.error("Ошибка конфигурации парсера: " + e);
        }
    }


    public void buildSetUsers(String fileName) {
        Document doc;
        try {
            doc = docBuilder.parse(fileName);
            Element root = doc.getDocumentElement();
            NodeList userList = root.getElementsByTagName("administrator");
            for (int i = 0; i < userList.getLength(); i++) {
                Element userElement = (Element) userList.item(i);
                Administrator admin = buildAdmin(userElement);
                users.add(admin);
            }
        } catch (IOException e) {
            LOG.error("File error or I/O error: " + e);
        } catch (SAXException e) {
            LOG.error("Parsing failure: " + e);
        } catch (LogicalException e) {
            LOG.error(e);
        }
    }

    private Administrator buildAdmin(Element adminElement) throws LogicalException {
        Administrator admin = new Administrator();
        admin.setName(adminElement.getAttribute("login"));
        admin.setId(Integer.parseInt(adminElement.getAttribute("id").substring(1)));
        admin.setContact(getElementTextContent(adminElement, "contact"));
        admin.setClientList(getClientListContent(adminElement, "clients"));
        admin.setPriseList(getPriseListContent(adminElement, "goods"));
        return admin;
    }

    private GoodsList getPriseListContent(Element element, String elementName) {
        GoodsList pList = new GoodsList();
        NodeList nList = element.getElementsByTagName("good");
        for (int i = 0; i < nList.getLength(); i++){
            Element good = (Element) nList.item(i);
            String name = good.getAttribute("name-good");
            Integer id = Integer.parseInt(good.getAttribute("id-good").substring(1));
            Integer prise = Integer.parseInt(getElementTextContent(good, "prise"));
            Integer sale = Integer.parseInt(getElementTextContent(good, "sale"));
            String info = getElementTextContent(good, "info");
            Integer quantity = Integer.parseInt(getElementTextContent(good, "quantity"));
            pList.add( new Good(id, name, prise, sale, info, quantity));
        }
        return pList;
    }

    private ClientList getClientListContent(Element element, String elementName) {
        ClientList cList = new ClientList();
        NodeList nList = element.getElementsByTagName("client");
        for (int i = 0; i < nList.getLength(); i++){
            Element client = (Element) nList.item(i);
            String clientName = client.getAttribute("name-client");
            Integer id = Integer.parseInt(client.getAttribute("id-client").substring(1));
            Integer budget = Integer.parseInt(getElementTextContent(client, "budget"));
            Boolean status = Boolean.parseBoolean(getElementTextContent(client, "status"));
            cList.add( new Client(id, clientName, budget, status));
        }
        return cList;
    }

    private static String getElementTextContent(Element element, String elementName) {
        NodeList nList = element.getElementsByTagName(elementName);
        Node node = nList.item(0);
        String text = node.getTextContent();
        return text;
    }
}
