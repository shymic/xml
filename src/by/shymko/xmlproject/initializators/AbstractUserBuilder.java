package by.shymko.xmlproject.initializators;


import by.shymko.xmlproject.user.User;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by Andrey on 10.03.2015.
 */
public abstract class AbstractUserBuilder {
        // protected так как к нему часто обращаются из подкласса
        protected Set<User> users;
        public AbstractUserBuilder() {
            users = new HashSet<User>();
        }
        public AbstractUserBuilder(Set<User> users) {
            this.users = users;
        }
        public Set<User> getUsers() {
            return users;
        }
        abstract public void buildSetUsers(String fileName);
}

