package by.shymko.xmlproject.initializators;

/**
 * Created by Andrey on 10.03.2015.
 */
public enum UserEnum {
    USERS, ADMINISTRATOR, LOGIN, ID, CONTACT, CLIENTS, CLIENT, ID_CLIENT, NAME_CLIENT, BUDGET, STATUS,
    GOODS, GOOD, NAME_GOOD, ID_GOOD, PRISE, SALE, INFO, QUANTITY
}
