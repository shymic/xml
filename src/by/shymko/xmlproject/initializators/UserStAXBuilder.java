package by.shymko.xmlproject.initializators;

import by.shymko.xmlproject.exceptions.LogicalException;
import by.shymko.xmlproject.goods.Good;
import by.shymko.xmlproject.list.ClientList;
import by.shymko.xmlproject.list.GoodsList;
import by.shymko.xmlproject.user.Administrator;
import by.shymko.xmlproject.user.Client;
import by.shymko.xmlproject.user.User;
import org.apache.log4j.Logger;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Andrey on 10.03.2015.
 */
public class UserStAXBuilder extends AbstractUserBuilder {
    private static final Logger LOG = Logger.getLogger(UserStAXBuilder.class);
    private HashSet<User> users = new HashSet<>();
    private XMLInputFactory inputFactory;

    public UserStAXBuilder() {
        inputFactory = XMLInputFactory.newInstance();
    }

    public Set<User> getUsers() {
        return users;
    }

    public void buildSetUsers(String fileName)  {
        XMLStreamReader reader = null;
        String name;
        try ( FileInputStream inputStream = new FileInputStream(new File(fileName))){

            reader = inputFactory.createXMLStreamReader(inputStream);
            while (reader.hasNext()) {
                int type = reader.next();
                if (type == XMLStreamConstants.START_ELEMENT) {
                    name = reader.getLocalName();
                    if (UserEnum.valueOf(name.toUpperCase()) == UserEnum.ADMINISTRATOR) {
                        Administrator admin = buildAdministrator(reader);
                        users.add(admin);
                    }
                }
            }
        } catch (XMLStreamException ex) {
            LOG.error("StAX parsing error! " + ex.getMessage());
        } catch (FileNotFoundException ex) {
            LOG.error("File " + fileName + " not found! " + ex);
        } catch (LogicalException | IOException e) {
            LOG.error(e);
        }
    }
    private Administrator buildAdministrator(XMLStreamReader reader) throws LogicalException {
        Administrator admin = new Administrator();
        admin.setName(reader.getAttributeValue(null, UserEnum.LOGIN.name().toLowerCase()));
        admin.setId(Integer.parseInt(reader.getAttributeValue(null, UserEnum.ID.name().toLowerCase()).substring(1))); // проверить на null
        String name;
        try {
            while (reader.hasNext()) {
                int type = reader.next();
                switch (type) {
                    case XMLStreamConstants.START_ELEMENT:
                        name = reader.getLocalName();
                        switch (UserEnum.valueOf(name.toUpperCase())) {
                            case CONTACT:
                                admin.setContact(getXMLText(reader));
                                break;
                            case CLIENTS:
                                admin.setClientList(getXMLClientList(reader));
                                break;
                            case GOODS:
                                admin.setPriseList(getXMLPriseList(reader));
                                break;
                        }
                        break;
                    case XMLStreamConstants.END_ELEMENT:
                        name = reader.getLocalName();
                        if (UserEnum.valueOf(name.toUpperCase()) ==
                                UserEnum.ADMINISTRATOR) {
                            return admin;
                        }
                        break;
                }
            }

            throw new LogicalException("Unknown element in tag");
        } catch (XMLStreamException e) {
            throw new LogicalException(e);
        }
    }

    private GoodsList getXMLPriseList(XMLStreamReader reader) throws LogicalException {
        GoodsList gList = new GoodsList();
        int type;
        String name;
        Good good = new Good();
        try {
            while (reader.hasNext()){
                type = reader.next();
                switch(type) {
                    case XMLStreamConstants.START_ELEMENT:
                        name = reader.getLocalName();
                        switch (UserEnum.valueOf(name.toUpperCase())) {
                            case GOOD:
                                good = new Good();
                                String nameGood = UserEnum.NAME_GOOD.name().replace('_', '-');
                                String idGood = UserEnum.ID_GOOD.name().replace('_', '-');
                                good.setName(reader.getAttributeValue(null, nameGood.toLowerCase()));
                                good.setId(Integer.parseInt(reader.getAttributeValue(null, idGood.toLowerCase()).substring(1)));
                                break;
                            case PRISE:
                                good.setPrise(Integer.parseInt(getXMLText(reader)));
                                break;
                            case SALE:
                                good.setSale(Integer.parseInt(getXMLText(reader)));
                                break;
                            case INFO:
                                good.setInfo(getXMLText(reader));
                                break;
                            case QUANTITY:
                                good.setQuantity(Integer.parseInt(getXMLText(reader)));
                                break;
                        }break;

                    case XMLStreamConstants.END_ELEMENT:
                        name = reader.getLocalName();
                        switch (UserEnum.valueOf(name.toUpperCase())){
                            case GOOD:
                                gList.add(good);
                                break;
                            case GOODS:
                                return gList;
                        }
                        break;
                }
            }
        } catch (XMLStreamException e) {
            throw new LogicalException(e);
        }
        throw new LogicalException("absent closed tag!");
    }

    private ClientList getXMLClientList(XMLStreamReader reader) throws  LogicalException {
        ClientList cList = new ClientList();
        int type;
        String name;
        Client client = new Client();
        try {
            while (reader.hasNext()){
                type = reader.next();
                switch(type) {
                    case XMLStreamConstants.START_ELEMENT:
                        name = reader.getLocalName();
                        switch (UserEnum.valueOf(name.toUpperCase())) {
                            case CLIENT:
                                client = new Client();
                                String nameClient = UserEnum.NAME_CLIENT.name().replace('_', '-');
                                String idClient = UserEnum.ID_CLIENT.name().replace('_', '-');
                                client.setName(reader.getAttributeValue(null, nameClient.toLowerCase()));
                                client.setId(Integer.parseInt(reader.getAttributeValue(null, idClient.toLowerCase()).substring(1)));
                                break;
                            case BUDGET:
                                client.setBudget(Integer.parseInt(getXMLText(reader)));
                                break;
                            case STATUS:
                                client.setActivity(Boolean.parseBoolean(getXMLText(reader)));
                                break;
                        }break;

                    case XMLStreamConstants.END_ELEMENT:
                        name = reader.getLocalName();
                        switch (UserEnum.valueOf(name.toUpperCase())){
                            case CLIENT:
                                cList.add(client);
                                break;
                            case CLIENTS:
                                return cList;
                        }
                        break;
                }

            }
        } catch (XMLStreamException e) {
            throw new LogicalException(e);
        }
        throw new LogicalException("absent closed tag!");
    }


    private String getXMLText(XMLStreamReader reader) throws XMLStreamException {
        String text = null;
        if (reader.hasNext()) {
            reader.next();
            text = reader.getText();
        }
        return text;
    }
}

