package by.shymko.xmlproject.initializators;

import by.shymko.xmlproject.user.User;
import org.apache.log4j.Logger;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Andrey on 12.03.2015.
 */
public class UserSAXBuilder extends AbstractUserBuilder {
    private static final Logger LOG = Logger.getLogger(UserSAXBuilder.class);
    private Set<User> users;
    private UserHandler uh;
    private XMLReader reader;
    public UserSAXBuilder() {
        uh = new UserHandler();
        try {
            reader = XMLReaderFactory.createXMLReader();
            reader.setContentHandler(uh);
        } catch (SAXException e) {
            LOG.error("ошибка SAX парсера: " + e);
        }
    }
    public Set<User> getUsers() {
        return users;
    }

    @Override
    public void buildSetUsers(String fileName) {
        try {
            Set<User> users = new HashSet<>();
            reader.parse(fileName);
        } catch (SAXException e) {
            LOG.error("ошибка SAX парсера: " + e);
        } catch (IOException e) {
            LOG.error("ошибка I/О потока: " + e);
        }
        users = uh.getUsers();
    }
}

