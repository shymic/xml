package by.shymko.xmlproject.initializators;

import by.shymko.xmlproject.exceptions.LogicalException;

/**
 * Created by Andrey on 10.03.2015.
 */
public class UsersBuilderFactory {
    private enum TypeParser {
        SAX, STAX, DOM
    }
    public AbstractUserBuilder createUserBuilder(String typeParser) throws LogicalException {
        TypeParser type = TypeParser.valueOf(typeParser.toUpperCase());
        switch (type) {
            case DOM:
                return new UserDOMBuilder();
            case STAX:
                return new UserStAXBuilder();
           case SAX:
                return new UserSAXBuilder();
            default:
                throw new LogicalException();
        }
    }
}
