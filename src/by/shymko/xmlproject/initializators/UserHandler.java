package by.shymko.xmlproject.initializators;

import by.shymko.xmlproject.exceptions.LogicalException;
import by.shymko.xmlproject.goods.Good;
import by.shymko.xmlproject.list.ClientList;
import by.shymko.xmlproject.list.GoodsList;
import by.shymko.xmlproject.user.Administrator;
import by.shymko.xmlproject.user.Client;
import by.shymko.xmlproject.user.User;
import org.apache.log4j.Logger;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

import java.util.EnumSet;
import java.util.HashSet;
import java.util.Set;


/**
 * Created by Andrey on  12.03.2015.
 */
public class UserHandler extends DefaultHandler{
    private static final Logger LOG = Logger.getLogger(UserHandler.class);
    private Set<User> users;
    private Administrator current = null;
    private Good currentGood = null;
    private GoodsList currentGoodList = null;
    private Client currentClient = null;
    private ClientList currentClientList = null;
    private UserEnum currentEnum = null;
    private EnumSet<UserEnum> withText;
    public UserHandler() {
        users = new HashSet<User>();
        withText = EnumSet.range(UserEnum.LOGIN, UserEnum.CLIENTS);
    }

    public UserHandler(Set<User> users) {
        this.users = users;
    }

    public Set<User> getUsers() {
        return users;
    }

    public void startElement(String uri, String localName, String qName, Attributes attrs) {
        try {
            UserEnum temp  = UserEnum.valueOf(localName.toUpperCase());
            switch (temp) {
                case ADMINISTRATOR:
                    current = new Administrator();
                    current.setName(attrs.getValue(0));
                    current.setId(Integer.parseInt(attrs.getValue(1).substring(1)));
                    break;
                case GOODS:
                    currentGoodList = new GoodsList();
                    break;
                case GOOD:
                    currentGood = new Good();
                    currentGood.setName(attrs.getValue(0));
                    currentGood.setId(Integer.parseInt(attrs.getValue(1).substring(1)));
                    break;
                case CLIENTS:
                    currentClientList = new ClientList();
                    break;
                case  CLIENT:
                    currentClient = new Client();
                    currentClient.setName(attrs.getValue(0));
                    currentClient.setId(Integer.parseInt(attrs.getValue(1).substring(1)));
                    break;
                default:
                    if ( withText.contains(temp)){
                        currentEnum = temp;
                    }
                    break;
            }
        }catch (LogicalException e) {
            LOG.error(e);
        }
    }
    public void endElement(String uri, String localName, String qName) {
        try {
            UserEnum temp  = UserEnum.valueOf(localName.toUpperCase());
            switch (temp) {
                case ADMINISTRATOR:
                    users.add(current);
                    break;
                case GOODS:
                    current.setPriseList(currentGoodList);
                    break;
                case GOOD:
                    currentGoodList.add(currentGood);
                    break;
                case CLIENTS:
                    current.setClientList(currentClientList);
                    break;
                case  CLIENT:
                    currentClientList.add(currentClient);
                    break;
            }
        }
        catch ( LogicalException e){
            LOG.error(e);
        }

    }
    public void characters(char[] ch, int start, int length) {
        String s = new String(ch, start, length).trim();
        try {
            if (currentEnum != null) {
                switch (currentEnum) {
                    case CONTACT:
                        current.setContact(s);
                        break;
                    case BUDGET:
                        currentClient.setBudget(Integer.parseInt(s));
                        break;
                    case STATUS:
                        currentClient.setActivity(Boolean.parseBoolean(s));
                        break;
                    case PRISE:
                        currentGood.setPrise(Integer.parseInt(s));
                        break;
                    case QUANTITY:
                        currentGood.setQuantity(Integer.parseInt(s));
                        break;
                    case SALE:
                        currentGood.setSale(Integer.parseInt(s));
                        break;
                    case INFO:
                        currentGood.setInfo(s);
                        break;
                    default:
                        throw new EnumConstantNotPresentException(
                                currentEnum.getDeclaringClass(), currentEnum.name());
                }
            }

            currentEnum = null;
        }catch( LogicalException e) {
            LOG.error(e);
        }
    }
}
