package by.shymko.xmlproject.comparator.usercomparator;

import by.shymko.xmlproject.user.User;

import java.util.Comparator;

/**
 * Created by Andrey on 22.02.2015.
 */
public class NameComparator implements Comparator<User> {

    @Override
    public int compare(User o1, User o2) {
        return o1.getName().compareTo(o2.getName());
    }
}
