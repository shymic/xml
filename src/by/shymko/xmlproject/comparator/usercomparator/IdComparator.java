package by.shymko.xmlproject.comparator.usercomparator;

import by.shymko.xmlproject.user.User;

import java.util.Comparator;

/**
 * Created by Andrey on 22.02.2015.
 */
public class IdComparator implements Comparator<User> {
    @Override
    public int compare(User o1, User o2) {
        return Integer.compare(o1.getId(), o2.getId());
    }
}
