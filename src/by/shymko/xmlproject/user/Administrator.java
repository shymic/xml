package by.shymko.xmlproject.user;


import by.shymko.xmlproject.exceptions.LogicalException;
import by.shymko.xmlproject.goods.Good;
import by.shymko.xmlproject.list.ClientList;
import by.shymko.xmlproject.list.GoodsList;
import org.apache.log4j.Logger;


/**
 * Created by Andrey on 17.02.2015.
 */
public class Administrator extends User {
    private static final Logger LOG = Logger.getLogger(Administrator.class);
    private String contact;
    private GoodsList priseList;
    private ClientList clientList;

    public Administrator() {
    }

    public Administrator(int id, String name) {
        super(id, name);
    }

    public Administrator(int id, String name, String contact,
                         ClientList clientList, GoodsList priseList) {
        super(id, name);
        this.contact = contact;
        this.priseList = priseList;
        this.clientList = clientList;
    }

    public Administrator(int id, String name, String contact) {
        super(id, name);
        this.contact = contact;
        priseList = new GoodsList();
        clientList = new ClientList();
    }


    public String getContact() {
        return contact;
    }

    public void setContact(String contact) throws LogicalException {
        if (contact.length() == 0) {
            throw new LogicalException("invalid contact");
        }
        this.contact = contact;
    }

    public GoodsList getPriseList() {
        return priseList;
    }

    public void setPriseList(GoodsList priseList) throws LogicalException {
        if (priseList == null) {
            throw new LogicalException("prise list is null");
        }
        this.priseList = priseList;
    }

    public ClientList getClientList() {
        return clientList;
    }

    public void setClientList(ClientList clientList) throws LogicalException {
        if (clientList == null) {
            throw new LogicalException("client list is null");
        }
        this.clientList = clientList;
    }

    public boolean addClient(Client client) {
        return clientList.add(client);
    }

    public boolean removeGood(int id) {
        return getPriseList().removeById(id);
    }

    public boolean addGood(Good good) {
        return priseList.add(good);
    }

    public boolean blockClient(int id) {
        if (clientList.findById(id) != null) {
            clientList.findById(id).setActivity(false);
            LOG.info(" client was blocked");
            return true;
        } else {
            LOG.info("id not found");
            return false;
        }
    }

    public boolean unblockClient(int id) {
        if (clientList.findById(id) != null) {
            clientList.findById(id).setActivity(true);
            LOG.info(" client was unblocked");
            return true;
        } else {
            LOG.info("id not found");
            return false;
        }
    }

    @Override
    public String toString() {
        return "Administrator{" + "id=" + super.getId() + '\n' +
                ", name='" + super.getName() + '\n' +
                "contact='" + contact + '\n' +
                ", priseList=" + priseList + '\n' +
                ", clientList=" + clientList + '\n' +
                '}';
    }
}
