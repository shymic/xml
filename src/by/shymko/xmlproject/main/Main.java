package by.shymko.xmlproject.main;

import by.shymko.xmlproject.comparator.usercomparator.IdComparator;
import by.shymko.xmlproject.comparator.usercomparator.NameComparator;
import by.shymko.xmlproject.exceptions.LogicalException;
import by.shymko.xmlproject.exceptions.TecnicalException;
import by.shymko.xmlproject.initializators.UserSAXBuilder;
import by.shymko.xmlproject.initializators.UserStAXBuilder;
import by.shymko.xmlproject.initializators.UsersBuilderFactory;
import by.shymko.xmlproject.internetshop.Shop;
import by.shymko.xmlproject.user.Administrator;
import by.shymko.xmlproject.user.User;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

import java.util.Set;

/**
 * Created by Andrey on 17.02.2015.
 */
public class Main {
    private static final Logger LOG = Logger.getLogger(Main.class);

    static {
        new DOMConfigurator().doConfigure("resourses\\log4j.xml", LogManager.getLoggerRepository());
    }

    public static void main(String[] args) {
        try {
            LOG.info("Start");
            UsersBuilderFactory fac = new UsersBuilderFactory();
            UserSAXBuilder builderSAX = (UserSAXBuilder) fac.createUserBuilder("sax");
            builderSAX.buildSetUsers("resourses\\data.xml");
            Set<User> users = builderSAX.getUsers();
            for (Object user : users.toArray()) {
                Shop shop = new Shop((Administrator) user);
                System.out.println(shop.getAdmin().toString());
            }
            LOG.debug("finish");
        } catch (LogicalException e) {
            LOG.error(e);
        }

    }
}
