package by.shymko.xmlproject.list;


import by.shymko.xmlproject.user.Client;

import java.util.ArrayList;

/**
 * Created by Andrey on 17.02.2015.
 */
public class ClientList extends ArrayList<Client> {
    public ClientList(int initialCapacity) {
        super(initialCapacity);
    }

    public ClientList() {
    }

    public Client findById(int id) {
        for (int i = 0; i < this.size(); ++i) {
            if (this.get(i).getId() == id) {
                return this.get(i);
            }
        }
        return null;
    }


}
