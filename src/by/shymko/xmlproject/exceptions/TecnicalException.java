package by.shymko.xmlproject.exceptions;

/**
 * Created by Andrey on 22.02.2015.
 */
public class TecnicalException extends Exception {
    public TecnicalException() {
    }

    public TecnicalException(String message) {
        super(message);
    }

    public TecnicalException(String message, Throwable cause) {
        super(message, cause);
    }

    public TecnicalException(Throwable cause) {
        super(cause);
    }

    public TecnicalException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
