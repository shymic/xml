package by.shymko.xmlproject.internetshop;

import by.shymko.xmlproject.exceptions.LogicalException;
import by.shymko.xmlproject.exceptions.TecnicalException;
import by.shymko.xmlproject.user.Administrator;
import by.shymko.xmlproject.user.User;

import java.util.ArrayList;
import java.util.Set;

/**
 * Created by Andrey on 17.02.2015.
 */
public class Shop {
    private ArrayList<User> staff;
    private Administrator admin;

    public Shop(Administrator admin) {
        this.admin = admin;
        initStaff();
    }

    public Shop() throws TecnicalException, LogicalException {
        //admin = ReadData.readData(DATA);

        initStaff();
    }

    public Administrator getAdmin() {
        return admin;
    }

    public void initStaff() {
        staff = new ArrayList<>();
        staff.add(admin);
        staff.addAll(admin.getClientList());
    }

    @Override
    public String toString() {
        return "Shop{" +
                "staff=" + staff +
                ", admin=" + admin +
                '}';
    }
}